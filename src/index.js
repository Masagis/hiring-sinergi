import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

import seWorker from './swRegister'

ReactDOM.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.getElementById('root')
)
seWorker()
