import React, { useState, useEffect } from 'react'

import MenuTables from '../MenuTables'

const Datatables = [
	{
		id: 1,
		name: 'Table 01',
		category: 'Category 01',
		availability: 'Available',
		arrival: 'Arrived',
	},
	{
		id: 2,
		name: 'Table 02',
		category: 'Category 02',
		availability: 'FULL',
		arrival: "Hasn't Arrived",
	},
	{
		id: 3,
		name: 'Table 03',
		category: 'Category 03',
		availability: 'Available',
		arrival: 'Arrived',
	},
	{
		id: 4,
		name: 'Table 04',
		category: 'Category 04',
		availability: 'FULL',
		arrival: 'Arrived',
	},
]

function Tables() {
	const [data, setData] = useState([])
	const [showModalSelect, setShowModalSelect] = useState(false)
	const [checklist, setChecklist] = useState([])

	useEffect(() => {
		setData(Datatables)
	}, [])

	const handleChange = (e) => {
		const { name, checked } = e.target
		let tempCheck = []

		if (name === 'selectAll') {
			tempCheck = data.map((g) => {
				return { ...g, isChecked: checked }
			})

			setData(tempCheck)
		} else {
			tempCheck = data.map((g) => (g.name === name ? { ...g, isChecked: checked } : g))

			setData(tempCheck)
		}
	}

	const handleChecklist = () => {
		let isTableCecked = data.filter((ind) => ind.isChecked === true)
		if (isTableCecked.length > 0) {
			setShowModalSelect(true)
		} else {
			setShowModalSelect(false)
		}
		return isTableCecked
	}

	useEffect(() => {
		setChecklist(handleChecklist())
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [data])

	return (
		<div className='section--table'>
			<table className='table-title'>
				<thead>
					<tr>
						<th>
							<span className='custom-checkbox'>
								<input
									type='checkbox'
									name='selectAll'
									checked={data.filter((g) => g?.isChecked !== true).length < 1}
									onChange={handleChange}
								/>
								<label></label>
							</span>
						</th>
						<th>Name</th>
						<th>Category</th>
						<th>Availability</th>
						<th>Arrival</th>
					</tr>
				</thead>
				<tbody>
					{data.map((items) => (
						<tr key={items.id}>
							<td>
								<span className='custom-checkbox'>
									<input
										type='checkbox'
										name={items.name}
										checked={items?.isChecked || false}
										onChange={handleChange}
									/>
									<label></label>
								</span>
							</td>
							<td>{items.name}</td>
							<td>{items.category}</td>
							<td>{items.availability}</td>
							<td>{items.arrival}</td>
						</tr>
					))}
				</tbody>
			</table>
			{showModalSelect && (
				<MenuTables
					data={data}
					setData={setData}
					setShowModalSelect={setShowModalSelect}
					checklist={checklist}
				/>
			)}
		</div>
	)
}

export default Tables
