import React, { useState } from 'react'

function InfoCookiess() {
	const [showHide, setShowHide] = useState('section-cookies')

	const handleOk = () => {
		document.cookie = 'username=masagis; max-age=' + 60

		if (document.cookie) {
			setShowHide('section-cookies-hide')
		}
	}

	return (
		<div className={showHide}>
			<img src='/images/info.svg' alt='info cookies' className='img-fluid' />
			<h4 style={{ color: '#4f4f4f', fontSize: '15px', fontWeight: '400' }}>
				This website uses cookies
			</h4>
			<button className='btn btn-hide' onClick={handleOk}>
				OK
			</button>
		</div>
	)
}

export default InfoCookiess
