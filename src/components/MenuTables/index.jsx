import React from 'react'

function index({ data, setData, setShowModalSelect, checklist }) {
	const handleClose = () => {
		setShowModalSelect((prev) => !prev)
	}

	const handleDelete = () => {
		const deleteData = data.filter((user) => user.isChecked !== true)
		setData(deleteData)
	}

	return (
		<div className='modal-float'>
			<img
				src='/images/closex.svg'
				alt='svg close'
				onClick={handleClose}
				style={{ cursor: 'pointer', width: 18, height: 18 }}
			/>
			<h3 style={{ font: 'normal normal 18px/24px Poppins' }}>
				{checklist.length} Table Selected
			</h3>
			<div className='d-flex'>
				<button className='btn btn-primary'>
					<img src='/images/assign.svg' alt='svg assign' /> Assign Category
				</button>
				&nbsp;
				<button className='btn btn-secondary' onClick={handleDelete}>
					<img src='/images/delete.svg' alt='svg delete' /> Delete Table
				</button>
			</div>
		</div>
	)
}

export default index
