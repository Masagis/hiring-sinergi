import React from 'react'

const Offline = ({ setMode }) => {
	return (
		<div className='offline-mode'>
			<div className='offline-modal-mode'>
				<button className='btn btn-close' onClick={() => setMode('online')}>
					X
				</button>
				<img src='offline.png' alt='No Connection' className='img-fluid img-center' />
				<h1>No internet connection</h1>
				<p>Seems like you're not connected to the internet!</p>
				<p>Check your connection and refresh the page.</p>
			</div>
		</div>
	)
}

export default Offline
