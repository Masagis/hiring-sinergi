import React from 'react'

function CardChartTwo() {
	return (
		<React.Fragment>
			<svg
				width='392'
				height='146'
				fill='none'
				xmlns='https://www.w3.org/2000/svg'
				viewBox='20 7 25 28'
			>
				<line x1='5' y1='30' x2='5' y2='15' className='bar-charts15' />
				<line x1='10' y1='30' x2='10' y2='15' className='bar-charts15' />
				<line x1='15' y1='30' x2='15' y2='20' className='bar-charts10' />
				<line x1='20' y1='30' x2='20' y2='15' className='bar-charts11' />
				<line x1='25' y1='30' x2='25' y2='12' className='bar-charts18' />
				<line x1='30' y1='30' x2='30' y2='14' className='bar-charts14' />
				<line x1='35' y1='30' x2='35' y2='10' className='bar-charts20' />
				<line x1='40' y1='30' x2='40' y2='11' className='bar-charts19' />
				<line x1='45' y1='30' x2='45' y2='18' className='bar-charts12' />
				<line x1='50' y1='30' x2='50' y2='20' className='bar-charts10' />
				<line x1='55' y1='30' x2='55' y2='15' className='bar-charts20' />
				<line x1='60' y1='30' x2='60' y2='18' className='bar-charts16' />
			</svg>
		</React.Fragment>
	)
}

export default CardChartTwo
