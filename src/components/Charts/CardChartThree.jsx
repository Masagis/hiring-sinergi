import React from 'react'

function CardChartThree() {
	return (
		<React.Fragment>
			<svg
				className='pie-chart'
				width='180'
				height='180'
				xmlns='https://www.w3.org/2000/svg'
				viewBox='0 0 180 180'
			>
				<circle stroke='#F7E8E5' strokeWidth='20' fill='none' cx='90' cy='90' r='75' />
				<circle
					className='pie-chart_base'
					stroke='#E66E3B'
					strokeWidth='20'
					strokeDasharray='330,471'
					strokeLinecap='round'
					fill='none'
					cx='90'
					cy='90'
					r='75'
				/>
				<g className='pie-chart_front'>
					<text
						className='pie-chart_percent'
						x='90'
						y='90'
						alignmentBaseline='central'
						textAnchor='middle'
						fontSize='18px'
						fill='#E66E3B'
					>
						70%
					</text>
				</g>
			</svg>
		</React.Fragment>
	)
}

export default CardChartThree
