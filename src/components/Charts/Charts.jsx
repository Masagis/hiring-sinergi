import React from 'react'

import ChartOne from './CardChartOne'
import ChartTwo from './CardChartTwo'
import ChartThree from './CardChartThree'

function index() {
	return (
		<div className='row section-charts'>
			<div className='col-md-4'>
				<div className='cardbox-chart'>
					<h3 style={{ fontSize: '18px', marginBottom: '25px' }}>Chart 1</h3>
					<div className='cardbox-wrapper'>
						<div className='month-horizontal'>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								May
							</h5>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								Apr
							</h5>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								Mar
							</h5>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								Feb
							</h5>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								Jan
							</h5>
						</div>
						<div className='chart-svg'>
							<ChartOne />
						</div>
						<div className='num-vertical'>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								01
							</h5>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								02
							</h5>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								03
							</h5>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								04
							</h5>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								05
							</h5>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								06
							</h5>
							<h5 style={{ color: '#647186', fontSize: '12px', fontWeight: '400' }}>
								07
							</h5>
						</div>
					</div>
				</div>
			</div>
			<div className='col-md-4'>
				<div className='cardbox-chart'>
					<h3 style={{ fontSize: '18px', marginBottom: '25px' }}>Chart 2</h3>
					<div className='cardbox-wrapper'>
						<ChartTwo />
					</div>
					<div className='bar-chart-data'>
						<svg className='bulletin1' width='14' height='14'>
							<circle fill='#C2D9D7' cx='7' cy='7' r='7' />
						</svg>
						&nbsp;
						<h4
							style={{
								fontSize: '12px',
								fontWeight: '400',
								color: '#647186',
								marginRight: '10px',
							}}
						>
							Text 1
						</h4>
						<svg className='bulletin1' width='14' height='14'>
							<circle fill='#315858' cx='7' cy='7' r='7' />
						</svg>
						&nbsp;
						<h4 style={{ fontSize: '12px', fontWeight: '400', color: '#647186' }}>
							Text 2
						</h4>
					</div>
				</div>
			</div>
			<div className='col-md-4'>
				<div className='cardbox-chart'>
					<h3 style={{ fontSize: '18px', marginBottom: '25px' }}>Chart 3</h3>
					<div className='cardbox-wrapper'>
						<ChartThree />

						<div className='pie-chart-data'>
							<h4
								style={{
									fontSize: '15px',
									fontWeight: '400',
									color: '#242933',
									marginTop: 35,
								}}
							>
								Text 1
							</h4>
							<p
								style={{
									fontSize: '15px',
									fontWeight: '400',
									color: '#566376',
									marginTop: 5,
								}}
							>
								40 Guest(s)
							</p>
							<svg className='bulletin1' width='14' height='14'>
								<circle fill='#E66E3B' cx='7' cy='7' r='7' />
							</svg>
							<hr />
							<h4 style={{ fontSize: '15px', fontWeight: '400', color: '#242933' }}>
								Text 2
							</h4>
							<p
								style={{
									color: '#566376',
									marginTop: 5,
									fontSize: '15px',
									fontWeight: '400',
								}}
							>
								210 Guest(s)
							</p>
							<svg className='bulletin2' width='14' height='14'>
								<circle fill='#F7E8E5' cx='7' cy='7' r='7' />
							</svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default index
