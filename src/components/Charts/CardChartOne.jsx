import React from 'react'

function CardChartOne() {
	return (
		<React.Fragment>
			<svg
				width='392'
				height='146'
				fill='none'
				xmlns='https://www.w3.org/2000/svg'
				viewBox='23 7 25 28'
			>
				<line x1='0' y1='8.3' x2='53' y2='8.3' className='line-base' />
				<line x1='0' y1='14.8' x2='53' y2='14.8' className='line-base' />
				<line x1='0' y1='21.3' x2='53' y2='21.3' className='line-base' />
				<line x1='0' y1='27.4' x2='53' y2='27.4' className='line-base' />
				<line x1='0' y1='33.3' x2='53' y2='33.3' className='line-base' />
				{/* Line front */}
				<line x1='0' y1='8.3' x2='35' y2='8.3' className='line-front' />
				<line x1='0' y1='14.8' x2='20' y2='14.8' className='line-front' />
				<line x1='0' y1='21.3' x2='25' y2='21.3' className='line-front' />
				<line x1='0' y1='27.7' x2='15' y2='27.7' className='line-front' />
				<line x1='0' y1='33.3' x2='45' y2='33.3' className='line-front' />
			</svg>
		</React.Fragment>
	)
}

export default CardChartOne
