import React, { useState } from 'react'

import Charts from './components/Charts/Charts'
import Tables from './components/Table/Tables'

import './styles/index.scss'

import Offline from './components/ModalOffline/Offline'
import InfoCookies from './components/Cookies/InfoCookies'

function App() {
	const [mode, setMode] = useState('online')

	window.addEventListener('offline', (event) => {
		setMode('offline')
	})

	return (
		<React.Fragment>
			<InfoCookies />
			<div className='animation'>
				<div className='container'>
					<div className='top-header'>
						<h1 className='title-dashboard'>Charts and Table Visualization</h1>
					</div>
					<Charts />
					<Tables />
					{mode === 'offline' && <Offline setMode={setMode} />}
				</div>
			</div>
		</React.Fragment>
	)
}

export default App
