let cacheData = 'sw-cache'
this.addEventListener('Install', (event) => {
	event.waitUntil(
		caches.open(cacheData).then((cache) => {
			cache.addAll([
				'/static/js/vendors~main.chunk.js',
				'/static/js/0.chunk.js',
				'/static/js/bundle.js',
				'/index.html',
				'/',
				'/manifest.json',
				'/logo192.png',
				'/favicon.ico',
				'/offline.png',
			])
		})
	)
})

// eslint-disable-next-line no-restricted-globals
self.addEventListener('fetch', function (event) {
	if (!navigator.onLine) {
		event.respondWith(
			fetch(event.request).catch(function () {
				return caches.match(event.request)
			})
		)
	}
})
